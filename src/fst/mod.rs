use fsm::FSMWrap;

pub trait Transition<Next, Event, Err> {
    fn transition(&self, event: Event) -> Result<Next, Err>;
}

impl<F, In, Out, Err> Transition<Out, In, Err> for F
where
    F: Fn(In) -> Result<Out, Err>,
{
    fn transition(&self, event: In) -> Result<Out, Err> {
        self(event)
    }
}

pub trait FST<Input> {
    type Output;
    type State;
    type Err;

    fn is_initial(&self) -> bool;
    fn is_final(&self) -> bool;
    fn unwrap(self) -> Self::State;
    fn step(&mut self, inp: Option<Input>) -> Result<Option<Self::Output>, Self::Err>;
    fn to_fsm(self) -> FSMWrap<Self>
    where
        Self: Sized,
    {
        FSMWrap::new(self)
    }
    fn chain<F, E>(self, other: F) -> Chain<Self, F>
    where
        F: FST<Input, State = Self::State, Output = Self::Output, Err = E>,
        E: From<Self::Err>,
        Self: Sized,
    {
        Chain {
            f1: self,
            f2: other,
        }
    }

    fn compose<F, E, S, O>(self, other: F) -> Plex<Self, F>
    where
        F: FST<Self::Output, State = S, Output = O, Err = E>,
        E: From<Self::Err>,
        Self: Sized,
    {
        Plex {
            f1: self,
            f2: other,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Chain<F1, F2> {
    f1: F1,
    f2: F2,
}

impl<Input, Err2, F1, F2> FST<Input> for Chain<F1, F2>
where
    F1: FST<Input>,
    F2: FST<Input, State = F1::State, Output = F1::Output, Err = Err2>,
    Err2: From<F1::Err>,
{
    type State = F1::State;
    type Output = F1::Output;
    type Err = Err2;

    fn is_initial(&self) -> bool {
        self.f1.is_initial()
    }

    fn is_final(&self) -> bool {
        self.f2.is_final()
    }

    fn unwrap(self) -> Self::State {
        if !self.f1.is_final() {
            self.f1.unwrap()
        } else {
            self.f2.unwrap()
        }
    }

    fn step(&mut self, inp: Option<Input>) -> Result<Option<Self::Output>, Err2> {
        let out = if !self.f1.is_final() {
            self.f1.step(inp)?
        } else {
            self.f2.step(inp)?
        };

        Ok(out)
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Plex<F1, F2> {
    f1: F1,
    f2: F2,
}

impl<I1, F1, F2, E2> FST<I1> for Plex<F1, F2>
where
    F1: FST<I1>,
    F2: FST<F1::Output, Err = E2>,
    E2: From<F1::Err>,
{
    type State = (F1::State, F2::State);
    type Err = E2;
    type Output = F2::Output;

    fn is_initial(&self) -> bool {
        self.f1.is_initial()
    }
    fn is_final(&self) -> bool {
        self.f2.is_final()
    }
    fn unwrap(self) -> Self::State {
        (self.f1.unwrap(), self.f2.unwrap())
    }

    fn step(&mut self, inp: Option<I1>) -> Result<Option<Self::Output>, E2> {
        let out = self.f2.step(self.f1.step(inp)?)?;

        Ok(out)
    }
}

#[macro_export]
macro_rules! fst {
    ( 
        ($($meta:meta),*) $name:ident = {
        error: $error:ty,
        output: $output:ty,
        ($($ev_meta:meta),*) $events:ident = {$($ev_name:ident = $ev_type:ty),+}, 
        ($($st_meta:meta),*) $states:ident = {$($state_name:ident = $state:ty),+},
        initial: [$($init:tt),+],
        terminal: [$($terminal:tt),+],
        transitions: {$($from:ty => $to:ty; input $event:ty; with error $err:ty; via $clo:expr),+},
        step: $step:expr
    }) => {
            #[derive($($meta,)*)]
            pub struct $name {
                state: $states,
            }

            impl $crate::FST<$events> for $name {
                type Err = $error;
                type Output = $output;
                type State = $states;

                fn is_initial(&self) -> bool {
                    match self.state {
                        $(
                            $states::$init(_) => true,
                        )+
                        _ => false
                    }
                }

                fn is_final(&self) -> bool {
                    match self.state {
                        $(
                            $states::$terminal(_) => true,
                        )+
                        _ => false
                    }
                }

                fn unwrap(self) -> $states {
                    self.state
                }

                // #[allow(unused_variables)]
                fn step(&mut self, input: Option<$events>) -> Result<Option<$output>, $error> {
                    $step(self, input)
                }
            }

            #[derive($($ev_meta,)*)]
            pub enum $events {
                $(
                    $ev_name($ev_type),
                )+
            }

            #[derive($($st_meta,)*)]
            pub enum $states {
                $(
                    $state_name($state),
                )+
            }

            $(
                impl From<$ev_type>  for $events  {
                    fn from(__it: $ev_type) -> $events {
                        $events::$ev_name(__it)
                    }
                }
            )+

            $(
                impl From<$state>  for $states  {
                    fn from(__it: $state) -> $states {
                        $states::$state_name(__it)
                    }
                }
            )+

            $(
                impl $crate::Transition<$to, $event, $err> for $from {
                    #[allow(unused_variables)]
                    fn transition(&self, event: $event) -> Result<$to, $err> {
                        $clo(self, event)
                    }
                }
            )+
    };
}

#[cfg(test)]
mod test;
