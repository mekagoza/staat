use fst::*;

pub trait FSM<Input> {
    type Err;
    type State;

    fn is_initial(&self) -> bool;

    fn is_final(&self) -> bool;

    fn unwrap(self) -> Self::State;

    fn step(&mut self, inp: Option<Input>) -> Result<(), Self::Err>;
}

#[derive(Debug, PartialEq)]
pub struct FSMWrap<F> {
    inner: F,
}

impl<F> FSMWrap<F> {
    pub fn new(inner: F) -> FSMWrap<F> {
        FSMWrap { inner }
    }
}

impl<Input, F, O> FSM<Input> for FSMWrap<F>
where
    F: FST<Input, Output = O>,
{
    type State = F::State;
    type Err = F::Err;

    fn is_initial(&self) -> bool {
        self.inner.is_initial()
    }

    fn is_final(&self) -> bool {
        self.inner.is_final()
    }

    fn unwrap(self) -> Self::State {
        self.inner.unwrap()
    }

    fn step(&mut self, inp: Option<Input>) -> Result<(), Self::Err> {
        let _ = self.inner.step(inp)?;
        Ok(())
    }
}

// #[derive(Debug, Copy, Clone, PartialEq)]
// pub struct Compose<F1, F2> {
//     f1: F1,
//     f2: F2,
// }

// impl<State, Input, Output, Err, Err2, F1, F2> FST<State, Input, Output, Err2>
//     for Compose<F1, F2, Err>
// where
//     F1: FST<State, Input, Output, Err>,
//     F2: FST<State, Input, Output, Err2>,
//     Err2: From<Err>,
// {
//     fn is_initial(&self) -> bool {
//         self.f1.is_initial()
//     }
//     fn is_final(&self) -> bool {
//         self.f2.is_final()
//     }
//     fn unwrap(self) -> State {
//         if !self.f1.is_final() {
//             self.f1.unwrap()
//         } else {
//             self.f2.unwrap()
//         }
//     }
//     fn state(&self) -> &State {
//         if !self.f1.is_final() {
//             self.f1.state()
//         } else {
//             self.f2.state()
//         }
//     }
//     fn step(&mut self, inp: Option<Input>) -> Result<Option<Output>, Err2> {
//         let out = if !self.f1.is_final() {
//             self.f1.step(inp)?
//         } else {
//             self.f2.step(inp)?
//         };

//         Ok(out)
//     }
// }
