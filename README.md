# Staat
A weird library for state machines

# Usage
Copy-pasted from `examples/silly.rs` which is copy-pasted from `src/fst/test.rs`:
```rust
#[macro_use]
extern crate staat;

use std::num::ParseIntError;

use staat::{Transition, FST};

#[derive(Debug, Clone)]
pub struct Start {
    x: String,
    y: String,
}

#[derive(Debug, Copy, Clone)]
pub struct HaveInts {
    x: i32,
    y: i32,
}

#[derive(Debug, Copy, Clone)]
pub struct Product(pub i32);

#[derive(Debug, Copy, Clone)]
pub struct Sum(pub i32);

#[derive(Debug, Copy, Clone)]
pub struct Done(pub i32);

#[derive(Debug, Copy, Clone)]
pub struct Finish;

#[derive(Debug, Copy, Clone)]
pub struct Parse;

#[derive(Debug, Copy, Clone)]
pub struct Multiply(pub i32);

#[derive(Debug, Copy, Clone)]
pub struct Add(pub i32);

fst!((Clone,Debug) Macht = {
    error: Error,
    output: Ev,
    (Copy,Clone,Debug) Event = {
        Parse = Parse,
        Multiply = Multiply,
        Add = Add,
        Finish = Finish
    },
    (Clone,Debug) State = {
        Start = Start,
        HaveInts = HaveInts,
        Product = Product,
        Sum = Sum,
        Done = Done
    },
    initial: [Start],
    terminal: [Done],
    transitions: {
        Start => HaveInts; input Parse; with error Error; via |this: &Start, _| {
            Ok(HaveInts {
                x: this.x.parse()?,
                y: this.y.parse()?,
            })
        },
        HaveInts => Sum; input Add; with error Error; via |this: &HaveInts, input| {
            let Add(z) = input;
            Ok(Sum(this.x + this.y + z))
        },
        HaveInts => Product; input Multiply; with error Error; via |this: &HaveInts, input| {
            let Multiply(ref z) = input;
            Ok(Product(this.x * this.y * z))
        },
        Sum => Done; input Finish; with error Error; via |this: &Sum, _| {
            Ok(Done(this.0))
        },
        Product => Done; input Finish; with error Error; via |this: &Product,_| {
            Ok(Done(this.0))
        }
    },
    step: |this: &mut Macht, input: Option<Event>| -> Result<Option<Ev>, Error> {
        let (next, out): (State, Option<_>) = match (&this.state, input) {
            (State::Start(start), Some(Event::Parse(e))) => (start.transition(e)?.into(), Some(Silly.into())),
            (State::HaveInts(h), Some(Event::Add(z))) => {
                let s: Sum = h.transition(z)?;
                (s.into(), Some(Silly.into()))
            }
            (State::HaveInts(h), Some(Event::Multiply(z))) => {
                let p: Product = h.transition(z)?;
                (p.into(), Some(Silly.into()))
            }
            (State::Product(p), Some(Event::Finish(e))) => (p.transition(e)?.into(), Some(Silly.into())),
            (State::Sum(s), Some(Event::Finish(e))) => (s.transition(e)?.into(), Some(Silly.into())),
            _ => return Err(Error::BadInput),
        };

        this.state = next;

        Ok(out)
    }
});

#[derive(Debug, Copy, Clone)]
pub struct Silly;

#[derive(Debug, Copy, Clone)]
pub struct S1;

#[derive(Debug, Copy, Clone)]
pub struct S2;

fst!((Clone, Debug) Sturm = {
    error: Error,
    output:(),
    (Copy, Clone, Debug) Ev = {
        Silly = Silly
    },
    (Copy, Clone, Debug) St = {
        Start = S1,
        End = S2
    },
    initial: [Start],
    terminal: [End],
    transitions: {
        S1 => S2; input Silly; with error Error; via |this: &S1, _| {
            Ok(S2)
        }
    },
    step: |this: &mut Sturm, input: Option<Ev>| -> Result<Option<()>, Error> {
        let (next, out): (St, Option<_>) = match (&this.state, input) {
            (St::Start(start), Some(Ev::Silly(e))) => (start.transition(e)?.into(), None),
            (St::End(_), Some(Ev::Silly(_))) => (S2.into(), None),
            _ => return Err(Error::BadInput),
        };

        this.state = next;

        Ok(out)
    }
}
);

impl Macht {
    pub fn new(x: &str, y: &str) -> Macht {
        Macht {
            state: Start {
                x: x.to_owned(),
                y: y.to_owned(),
            }.into(),
        }
    }
}

impl Sturm {
    pub fn new() -> Sturm {
        Sturm { state: S1.into() }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Error {
    BadInput,
    BadInt(ParseIntError),
}

impl From<ParseIntError> for Error {
    fn from(pie: ParseIntError) -> Error {
        Error::BadInt(pie)
    }
}

fn basic() {
    let mut m = Macht::new("2", "3");
    println!("start = {:?}", m);

    let cl = move || -> Result<Macht, Error> {
        let events = vec![Parse.into(), Multiply(4).into(), Finish.into()];

        for ev in events {
            m.step(Some(ev))?;
            println!("machine = {:?}", m);
        }

        Ok(m)
    };

    let end = cl();

    println!("end = {:?}", end);
}

fn double() {
    let m1 = Macht::new("2", "3");
    let m2 = Sturm::new();
    let mut m = m1.compose(m2);
    println!("start = {:?}", m);

    let cl = move || -> Result<_, Error> {
        let events = vec![Parse.into(), Multiply(4).into(), Finish.into()];

        for ev in events {
            m.step(Some(ev))?;
            println!("machine = {:?}", m);
        }

        Ok(m)
    };

    let end = cl();

    println!("end = {:?}", end);
}

fn main() {
    basic();

    double();
}

```